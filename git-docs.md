# Docs for git templates

## Git push

Pushes any commits you've made to the git repo.

### Simple (push to current repo & branch with current username)
```yml
include:
  - project: txlab/ci-templates # https://gitlab.com/txlab/ci-templates/
    file: git.gitlab-ci.yml
    # ref: vX.Y - use a tag with fixed version (pro tip: renovate bot will detect upgrades)

push changes:
  extends: .git_push
  script:
    - echo change > file.txt
    - git add -A
    - git commit -a -m "[ci] change" -m "$CI_JOB_URL"
  # push is done in after_script
```

### Override examples:
```yml
push changes:
  image: registry.gitlab.com/txlab/docker/windmill-cli # image needs git & ssh-client
  extends: .git_push
  variables:
    TARGET_BRANCH: dev-sync
  before_script:
    - !reference [.git_push, before_script]
    - git config user.email "gitlab-ci+project@your.org"
    - git config user.name "ci-sync"
  script:
    - wmill sync pull
    - git add -A
    - git commit -a -m "[ci] pull" -m "$CI_JOB_URL"
```