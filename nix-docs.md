# Nix deploy templates

## `nix_attic_build`

Builds a derivation & push to [attic server](https://docs.attic.rs/).

```yml
include:
  - project: txlab/ci-templates # https://gitlab.com/txlab/ci-templates/
    file: nix.gitlab-ci.yml
    # ref: vX.Y - use a tag with fixed version (pro tip: renovate bot will detect upgrades)

build:
  extends: .nix_attic_build # https://gitlab.com/txlab/ci-templates/-/blob/main/nix.gitlab-ci.yml#L15
```

## `nix_build_docker`

Builds a docker image based on [nix2container](https://github.com/nlewo/nix2container).

```yml
include:
  - project: txlab/ci-templates # https://gitlab.com/txlab/ci-templates/
    file: nix.gitlab-ci.yml
    # ref: vX.Y - use a tag with fixed version (pro tip: renovate bot will detect upgrades)

docker_build:
  extends: .nix_build_docker # https://gitlab.com/txlab/ci-templates/-/blob/main/nix.gitlab-ci.yml#L31
  tags:
    - nix # use a runner set up for nix caching
    - dind # docker-in-docker
```

#### Override defaults:
see [nix.gitlab-ci.yml](./nix.gitlab-ci.yml#L59)
