# Web3 (IPFS) deploy templates

## `publish_w3`

Pushes static files to IPFS storage & updates an IPNS record

### Set up variables
1. Set up a key & auth proof for storacha (follow this guide: https://web3.storage/docs/how-to/ci/)
    - put the `key` of `w3 key create --json` in **`W3_PRINCIPAL`**
    - put the base64-encoded proof of `w3 delegation create` in **`W3_PROOF`**
2. (optional, if you want to update an IPNS record) Generate an IPNS key for 
    - `nix shell github:yusefnapora/w3name-rust-client` or [install in a different way](https://github.com/yusefnapora/w3name-rust-client/blob/main/w3name-cli/README.md)
    - `w3name create`
    - Encode using base64: `base64 -w 0 k51foobar.key`
    - put the result in **`W3NAME_KEY`**

```yml
include:
  - project: txlab/ci-templates # https://gitlab.com/txlab/ci-templates/
    file: web3.gitlab-ci.yml
    # ref: vX.Y - use a tag with fixed version (pro tip: renovate bot will detect upgrades)

deploy:
  extends: .publish_w3 # https://gitlab.com/txlab/ci-templates/-/blob/main/web3.gitlab-ci.yaml?ref_type=heads#L1
  # Publish contents of dist/ 
```

#### Override defaults:
```yml
deploy:
  extends: .publish_w3 # https://gitlab.com/txlab/ci-templates/-/blob/main/web3.gitlab-ci.yaml?ref_type=heads#L1
  variables:
    DIST_DIR: dist/ # (default)
  script: # if you need to run scripts before, be sure to include the template's script:
    - mkdir dist
    - echo "$CI_COMMIT_SHA" > dist/commit.txt
    - !reference [.publish_w3, script]
```
