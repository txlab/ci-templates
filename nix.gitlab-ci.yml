.nix_build:
  stage: build
  interruptible: true
  image: nixpkgs/nix-flakes
  variables:
    DERIVATION: .
  script:
    - nix build -L $DERIVATION
  artifacts:
    paths:
      - result

###########
## CACHE ##
###########
.nix_attic:
  image: registry.gitlab.com/txlab/ci-templates/attic # prewarmed nixpkgs/nix-flakes with attic 
  before_script:
    - attic login server $ATTIC_SERVER $ATTIC_TOKEN
  
.nix_attic_build:
  extends: [ ".nix_build", ".nix_attic" ]
  script:
    - !reference [.nix_build, script]
    - attic push server:$ATTIC_CACHE ./result

############
## DOCKER ##
############
# - for nix2container-based outputs
# - for runners with a nix store mounted as cache
.nix_build_docker:
  stage: build
  interruptible: true
  image: nixpkgs/nix-flakes
  tags:
    - nix # use runner set up for nix
    - dind # docker-in-docker
  variables:
    NIX_OUTPUT: docker
    IMAGE_SUFFIX: '' # e.g. '/server'
    IMAGE_TAG: "$CI_COMMIT_REF_SLUG"
    REGISTRY_IMAGE: "${CI_REGISTRY_IMAGE}${IMAGE_SUFFIX}"
  rules:
    # TODO: Always tag with SHA
    # on default branch: tag with latest, otherwise branch name
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      variables:
        IMAGE_TAG: 'latest'
    - when: on_success # meaning: "else, nothing special"
  script:
    # HACK to tag with commit ref & latest, we just run the command twice (should be chroot)
    - >
      nix --store 'unix:///mnt/nix/var/nix/daemon-socket/socket?root=/mnt'
      run .#$NIX_OUTPUT.copyTo -- "docker://${REGISTRY_IMAGE}:${CI_COMMIT_SHA}"
      --dest-creds "${CI_REGISTRY_USER}:${CI_JOB_TOKEN}"
    - >
      nix --store 'unix:///mnt/nix/var/nix/daemon-socket/socket?root=/mnt'
      run .#$NIX_OUTPUT.copyTo -- "docker://${REGISTRY_IMAGE}:${IMAGE_TAG}"
      --dest-creds "${CI_REGISTRY_USER}:${CI_JOB_TOKEN}"
    - 'echo -e "Pushed to:\n- ${REGISTRY_IMAGE}:${CI_COMMIT_SHA}\n- ${REGISTRY_IMAGE}:${IMAGE_TAG}"'

#######################
## CACHE EXPERIMENTS ##
#######################
.nix_build_chroot:
  extends: .nix_build
  script:
    - nix --store .nix-chroot/ build -L $DERIVATION
  cache:
    key: nix
    paths:
      - .nix-chroot/
.nix-with-store-diff-cache:
  image: nixpkgs/nix-flakes:latest
  # variables:
  #   NIX_STORE_DIR: $CI_PROJECT_DIR/.nix-chroot/
  before_script:
    - du -hs /nix/store
    - "[[ -d .nix-store-cache/ ]] && du -hs .nix-store-cache/ && rsync -av .nix-store-cache/ /nix/store/"
    - ls /nix/store > /tmp/store-before
  after_script:
    # - "[ $CI_JOB_STATUS == 'success' ] || exit" # we don't want to run after-script if the job was not successfull
    - du -hs /nix/store
    - ls /nix/store > /tmp/store-after
    - comm -13 /tmp/store-before /tmp/store-after > /tmp/store-diff
    - rsync -av --include-from=/tmp/store-diff --exclude='*' /nix/store/ .nix-store-cache/
    - du -hs .nix-store-cache/
  cache:
    # key: nix # TODO: can we re-use the cache everywhere and just share one ?
    paths:
      - .nix-store-cache/
.nix-with-store-diff-cache-debug:
  extends: .nix-with-store-diff-cache
  after_script:
    - !reference [.nix-with-store-diff-cache, after_script]
    - mkdir debug
    - cp /tmp/store-* debug/
    - cp -r .nix-store-cache/ debug/
  artifacts:
    when: always
    expire_in: "7 days"
    paths:
      - debug/
